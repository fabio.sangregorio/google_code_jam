def compute_tidy_number(number, index):
    i = 0
    digits = [int(x) for x in number]

    if len(number) == 1:
        return f'Case #{index + 1}: {int(number)}\n'
    while i < len(number) - 1 and digits[i + 1] >= digits[i]:
        i += 1
        if i == len(number) - 1:
            return f'Case #{index + 1}: {int(number)}\n'
        continue

    digits[i+1:] = [9] * (len(digits) - i - 1)
    while digits[i - 1] == digits[i] and i > 0:
        digits[i] = 9
        i -= 1
    digits[i] -= 1

    number = ''.join([str(x) for x in digits])
    return f'Case #{index + 1}: {int(number)}\n'


with open('B-large-practice.in', 'r') as f:
    next(f)
    numbers_list = [x.replace('\n', '') for x in f.readlines()]

numbers_list = [compute_tidy_number(num, index) for index, num in enumerate(numbers_list)]

with open('output.txt', 'w') as f:
    f.writelines(numbers_list)